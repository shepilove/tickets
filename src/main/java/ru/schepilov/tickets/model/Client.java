package ru.schepilov.tickets.model;

import lombok.Data;
import ru.schepilov.tickets.enums.Gender;
import ru.schepilov.tickets.enums.UserRoles;

import javax.persistence.*;


@Data
@Entity
@Table(name = "client")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String surName;
    private Gender gender;
    private UserRoles role;
}
