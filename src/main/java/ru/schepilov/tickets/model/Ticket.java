package ru.schepilov.tickets.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    double cost;
    Date date;
    @ManyToOne
    @JoinColumn(name = "client_id", nullable = false)
    Client client;
}
