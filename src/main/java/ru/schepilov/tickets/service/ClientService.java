package ru.schepilov.tickets.service;

import org.springframework.http.ResponseEntity;
import ru.schepilov.tickets.model.Client;

public interface ClientService {

    ResponseEntity add(Client client);

    ResponseEntity getAllClient();

    ResponseEntity deleteClientById(long id);

    ResponseEntity updateClient(Client client);

    ResponseEntity getById(long id);

}
