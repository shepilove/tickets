package ru.schepilov.tickets.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.schepilov.tickets.dao.ClientRepository;
import ru.schepilov.tickets.enums.UserRoles;
import ru.schepilov.tickets.model.Client;
import ru.schepilov.tickets.service.ClientService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Override
    public ResponseEntity add(Client client) {
        Client checkClient = clientRepository.findByLogin(client.getLogin());
        if (checkClient != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Клиент с данным логином зарегистрирован ранее!");
        } else {
            client.setRole(UserRoles.USER);
            Client saveClient = clientRepository.save(client);
            return ResponseEntity.status(HttpStatus.OK).body(saveClient);
        }
    }

    @Override
    public ResponseEntity getAllClient() {
        List<Client> allClients = clientRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(allClients);
    }

    @Override
    public ResponseEntity getById(long id) {
        Optional<Client> searchClient = clientRepository.findById(id);
        if (searchClient.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(searchClient);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Клиент с id = " + id + " не найден!");
        }
    }

    @Override
    public ResponseEntity deleteClientById(long id) {
        try {
            clientRepository.deleteById(id);
            Map<String, String> message = new HashMap<>();
            message.put("message", "Клиент с id = " + id + " успешно удален!");
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Клиент с id = " + id + " не найден, а следовательно не может быть удален!");
        }
    }

    @Override
    public ResponseEntity updateClient(Client client) {
        Optional<Client> userFromBase = clientRepository.findById(client.getId());
        if (userFromBase.isPresent()) {
            Client updatedClient = clientRepository.save(client);
            return ResponseEntity.status(HttpStatus.OK).body(updatedClient);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("клиента с данным id не существует!");
        }
    }
}
