package ru.schepilov.tickets.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.schepilov.tickets.dao.TicketRepository;
import ru.schepilov.tickets.model.Client;
import ru.schepilov.tickets.model.Ticket;
import ru.schepilov.tickets.service.TicketService;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;

    @Override
    public ResponseEntity addTicket(Ticket ticket) {
        Optional<Ticket> ticketFromBase = ticketRepository.findById(ticket.getId());
        if (ticketFromBase.isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Данный билет уже существует!");
        } else {
            Ticket saveTicket = ticketRepository.save(ticket);
            return ResponseEntity.status(HttpStatus.OK).body(saveTicket);
        }
    }

    @Override
    public ResponseEntity deleteTicket(long id) {
        try {
            ticketRepository.deleteById(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Билета, который вы хотите удалить, не существует!");
        }
    }

    @Override
    public ResponseEntity getAll() {
        List<Ticket> allTickets = ticketRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(allTickets);
    }

    @Override
    public ResponseEntity findByClient(Client client) {
        return null;
    }
}
