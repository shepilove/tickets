package ru.schepilov.tickets.service;

import org.springframework.http.ResponseEntity;
import ru.schepilov.tickets.model.Client;
import ru.schepilov.tickets.model.Ticket;

public interface TicketService {

    ResponseEntity addTicket(Ticket ticket);

    ResponseEntity deleteTicket(long id);

    ResponseEntity getAll();

    ResponseEntity findByClient(Client client);
}
