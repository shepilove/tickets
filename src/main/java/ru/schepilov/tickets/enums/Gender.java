package ru.schepilov.tickets.enums;

public enum Gender {
    MALE,
    FEMALE
}
