package ru.schepilov.tickets.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.schepilov.tickets.model.Ticket;

import java.util.Date;
import java.util.List;

public interface TicketRepository extends JpaRepository<Ticket, Long> {

    List<Ticket> findByDateIsBefore(Date date);
}
