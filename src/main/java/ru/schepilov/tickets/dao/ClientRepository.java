package ru.schepilov.tickets.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.schepilov.tickets.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    Client findByLogin(String login);
}
