package ru.schepilov.tickets.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.schepilov.tickets.model.Client;
import ru.schepilov.tickets.service.impl.ClientServiceImpl;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("/clients")
//@PreAuthorize("USER")
public class ClientController {

    private final ClientServiceImpl clientServiceImpl;

    @PostMapping("/add")
    public ResponseEntity addClient(@RequestBody Client client) {
        return clientServiceImpl.add(client);
    }

    @GetMapping("/getAll")
    public ResponseEntity getAllClients() {
        return clientServiceImpl.getAllClient();
    }

    @GetMapping("/getById/{id}")
    public ResponseEntity getById(@PathVariable("id") long id){
        return clientServiceImpl.getById(id);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteClient(@PathVariable("id") long id) {
        return clientServiceImpl.deleteClientById(id);
    }

    @PutMapping("/update")
    public ResponseEntity updateClient(@RequestBody Client client) {
        return clientServiceImpl.updateClient(client);
    }
}
