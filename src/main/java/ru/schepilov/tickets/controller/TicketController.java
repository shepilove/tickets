package ru.schepilov.tickets.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.schepilov.tickets.model.Ticket;
import ru.schepilov.tickets.service.impl.TicketServiceImpl;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("/tickets")
public class TicketController {

    private final TicketServiceImpl ticketService;

    @PostMapping("/add")
    public ResponseEntity addTicket(@RequestBody Ticket ticket) {
        return ticketService.addTicket(ticket);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteTicket(@PathVariable("id") long id) {
        return ticketService.deleteTicket(id);
    }

    @GetMapping("/getAll")
    public ResponseEntity getAllClients() {
        return ticketService.getAll();
    }

}
