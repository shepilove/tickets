package ru.schepilov.tickets.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.schepilov.tickets.dao.ClientRepository;
import ru.schepilov.tickets.enums.UserRoles;
import ru.schepilov.tickets.model.Client;

@Controller
@RequiredArgsConstructor
@CrossOrigin
public class RegistrationController {

    private final ClientRepository clientRepository;

    @PostMapping("/register")
    public ResponseEntity registerClient(@RequestBody Client client) {

        Client clientFromDb = clientRepository.findByLogin(client.getLogin());
        if (clientFromDb != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Пользователь с таким логином уже существует!");
        }
        client.setRole(UserRoles.USER);
        clientRepository.save(client);
        return ResponseEntity.status(HttpStatus.OK).body(client);
    }

    @GetMapping(produces = "application/json")
    @RequestMapping("/login")
    public Client validateLogin() {
        return null;
    }
}
